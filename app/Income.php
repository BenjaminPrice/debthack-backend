<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Income extends Model
{
    protected $fillable = [
    	'name',
	    'amount',
	    'frequency'
    ];

    public function user()
    {
    	return $this->belongsTo('App\User');
    }
}
