<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Alternative extends Model
{
    protected $fillable = [
    	'title'
    ];

    public function flags()
    {
    	return $this->belongsToMany('App\Flag')->withPivot('savings_percent', 'message');
    }

    public function user()
    {
    	return $this->belongsToMany('App\User')->withPivot('ignore');
    }
}
