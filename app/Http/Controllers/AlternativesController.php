<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Flag;
use App\Transaction;
use App\User;
use App\Alternative;

class AlternativesController extends Controller
{
    public function retrieve($user_id)
    {
    	$flags = Flag::all();

    	for ($i = 0; $i < count($flags); $i++) {
    		$flags[$i]->alternatives = $flags[$i]->alternatives;
    		$flags[$i]->monthly_spend = $this->get_flag_amount($flags[$i]);
    		$flags[$i]->flag_exists = $this->user_flag_exists($user_id, $flags[$i]->id, $flags[$i]->alternatives);
    		$flags[$i]->ignored = $this->is_ignored($flags[$i]);

    		for ($x = 0; $x < count($flags[$i]->alternatives); $x++) {
    			$flags[$i]->alternatives[$x]->savings_dollar = $this->get_flag_amount($flags[$i]) * ($flags[$i]->alternatives[$x]->pivot->savings_percent / 100);
    			$flags[$i]->alternatives[$x]->savings_percent = $flags[$i]->alternatives[$x]->pivot->savings_percent;
    			$flags[$i]->alternatives[$x]->message = $flags[$i]->alternatives[$x]->pivot->message;
    		}
    	}

    	return $flags;
    }

    public function retrieve_new($user_id)
    {
    	$flags = Flag::all();

    	for ($i = 0; $i < count($flags); $i++) {
    		$flags[$i]->alternatives = $flags[$i]->alternatives;
    		$flags[$i]->monthly_spend = $this->get_flag_amount($flags[$i]);
    		$flags[$i]->flag_exists = $this->user_flag_exists($user_id, $flags[$i]->id, $flags[$i]->alternatives);
    		$flags[$i]->ignored = $this->is_ignored($flags[$i]);

    		for ($x = 0; $x < count($flags[$i]->alternatives); $x++) {
    			$flags[$i]->alternatives[$x]->savings_dollar = $this->get_flag_amount($flags[$i]) * ($flags[$i]->alternatives[$x]->pivot->savings_percent / 100);
    			$flags[$i]->alternatives[$x]->savings_percent = $flags[$i]->alternatives[$x]->pivot->savings_percent;
    			$flags[$i]->alternatives[$x]->message = $flags[$i]->alternatives[$x]->pivot->message;
    		}
    	}

    	$new_flags = array();

    	for ($i = 0; $i < count($flags); $i++) {
    		if (
    			!$flags[$i]->flag_exists &&
    			$flags[$i]->ignored == 0
    		) {
    			$new_flags[] = $flags[$i];
    		}
    	}

    	return response()->json([
    		'response' => 'success',
    		'flags' => $new_flags
    	]);
    }

    public function is_ignored($flag)
    {
    	foreach ($flag->alternatives as $alternative) {
    		if ($user = $alternative->user->first()) {
    			if ($user->pivot->ignore) {
    				return true;
    			}
    		} else {
    			continue;
    		}
    	}

    	return 'false';
    }

    public function flag_test()
    {
    	if ($this->flag_exists(1,3,2)) {
    		return 'exists';
    	} else {
    		return 'nope';
    	}
    }

    public function flag_exists($user_id, $alternative_id, $flag_id)
    {
    	$alternative = User::find($user_id)
    		->alternatives->where('id', $alternative_id)
    		->first();

    	if (null !== $alternative) {
    		return null !== $alternative->flags
    			->where('id', $flag_id)
    			->first();
    	} else {
    		return false;
    	}
    }

    public function user_flag_exists($user_id, $flag_id, $alternatives)
    {
    	foreach ($alternatives as $alternative) {
    		foreach ($alternative->user as $user) {
    			if ($user->id == $user_id) {
    				return true;
    			}
    		}
    	}

    	return false;
    }

    public function get_flag_amount($flag)
    {
    	$amount = 0;

    	foreach ($flag->keywords as $keyword) {
    		$amount += $this->get_keyword_amount($keyword->id);
    	}

    	return $amount;
    }

    public function get_keyword_amount($keyword_id)
    {
    	return Transaction::where('keyword_id', $keyword_id)
    	->where('postdate', '>', date('Y-m-d', strtotime('-6 weeks')))
    	->lists('amount')->sum();
    }

    public function assign_alternative(Request $request)
    {
    	if (
    		null !== $request->input('user_id') &&
    		null !== $request->input('alternative_id') &&
    		null !== $request->input('flag_id')
    	) {
    		if ($user = User::find($request->input('user_id'))) {
    			$user->alternatives()->attach($request->input('alternative_id'), ['flag_id' => $request->input('flag_id')]);

    			return response()->json([
    				'response' => 'success',
    				'assigned' => true
    			]);
    		} else {
    			return response()->json([
    				'response' => 'success',
    				'assigned' => false,
    				'message' => 'User not found...'
    			]);
    		}
    	} else {
    		return response()->json([
				'response' => 'success',
				'assigned' => false,
				'message' => 'Must provide user_id, alternative_id and flag_id as POST params.'
			]);
    	}
    }

    public function ignore_alternative(Request $request)
    {
    	if (
    		null !== $request->input('user_id') &&
    		null !== $request->input('alternative_id') &&
    		null !== $request->input('flag_id')
    	) {
    		if ($user = User::find($request->input('user_id'))) {
    			$user->alternatives()->attach($request->input('alternative_id'), ['flag_id' => $request->input('flag_id'), 'ignore' => true]);

    			return response()->json([
    				'response' => 'success',
    				'ignored' => true
    			]);
    		} else {
    			return response()->json([
    				'response' => 'success',
    				'ignored' => false,
    				'message' => 'User not found...'
    			]);
    		}
    	} else {
    		return response()->json([
				'response' => 'success',
				'ignored' => false,
				'message' => 'Must provide user_id, alternative_id and flag_id as POST params.'
			]);
    	}
    }

    public function user_alternatives($user_id)
    {
    	if ($user = User::find($user_id)) {
    		for ($i = 0; $i < count($user->alternatives); $i++) {
    			$user->alternatives[$i]->details = Alternative::find($user->alternatives[$i]->id)->flags()->first();
    		}

    		return response()->json([
    			'response' => 'success',
    			'alternatives' => $user->alternatives
    		]);
    	} else {
    		return response()->json([
    			'response' => 'success',
    			'alternatives' => false,
    			'message' => 'User not found...'
    		]);
    	}
    }
}
