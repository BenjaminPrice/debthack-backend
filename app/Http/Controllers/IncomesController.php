<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\Frequency;

class IncomesController extends Controller
{
    public function store(Request $request)
    {
    	if ($message = $this->account_validation_failed($request->all())) {
    		return response()->json([
    			'response' => 'success',
    			'valid' => false,
    			'message' => $message
    		]);
    	} else {
    		$user = User::find($request->input('user_id'));

    		$account = $user->income()->create([
    			'name' => $request->input('name'),
    			'amount' => $request->input('amount'),
    			'frequency' => $request->input('frequency'),
    		]);

    		return response()->json([
    			'response' => 'success',
    			'valid' => true,
    			'account' => $account
    		]);
    	}
    }

    public function get_user_income($user_id)
    {
    	if ($user = User::find($user_id)) {
    		$income = $user->income;

    		for ($i = 0; $i < count($income); $i++) {
    			$income[$i]->frequency = Frequency::find($income[$i]->frequency);
    		}

			return response()->json([
				'response' => 'success',
				'found' => true,
				'accounts' => $user->income
			]);
		} else {
			return response()->json([
				'response' => 'success',
				'found' => false
			]);
		}
    }

    public function account_validation_failed($data)
    {
    	return false;
    }
}
