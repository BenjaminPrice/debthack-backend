<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\DebtType;
use App\Frequency;

class TypesController extends Controller
{
    public function debts()
    {
    	return response()->json([
    		'response' => 'success',
    		'types' => DebtType::all()
    	]);
    }

    public function frequencies()
    {
    	return response()->json([
    		'response' => 'success',
    		'types' => Frequency::all()
    	]);
    }
}
