<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\Notification;

class NotificationsController extends Controller
{
    public function retrieve($user_id)
    {
    	return response()->json([
    		'response' => 'success',
    		'notifications' => User::find($user_id)->notifications
    	]);
    }

    public function flag(Request $request)
    {
    	$notification = Notification::find($request->input('notification_id'));
    	$notification->flagged = true;
    	$notification->save();

    	return response()->json([
    		'response' => 'success',
    		'notification' => $notification
    	]);
    }
}
