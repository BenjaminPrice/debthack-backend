<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class AccountsController extends Controller
{
    public function store(Request $request)
    {
    	if ($message = $this->account_validation_failed($request->all())) {
    		return response()->json([
    			'response' => 'success',
    			'valid' => false,
    			'message' => $message
    		]);
    	} else {
    		$user = User::find($request->input('user_id'));
    		return response()->json([
    			'response' => 'success',
    			'valid' => true,
    			'account' => 
    		]);
    	}
    }

	public function retrieve_user_accounts($user_id)
	{
		if ($user = User::find($user_id)) {
			return response()->json([
				'response' => 'success',
				'found' => true,
				'accounts' => $user->accounts
			]);
		} else {
			return response()->json([
				'response' => 'success',
				'found' => false
			]);
		}
	}

	public function account_validation_failed($data)
	{
		
	}
}
