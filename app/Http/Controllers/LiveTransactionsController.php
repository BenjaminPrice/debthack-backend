<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use DB;

class LiveTransactionsController extends Controller
{
    public function simulate_step_1(Request $request)
    {
    	$user = User::find($request->input('user_id'));

    	$user->live_transactions()->create([
    		'amount' => 12.60,
    		'keyword_id' => 5
    	]);

    	$user->live_transactions()->create([
    		'amount' => 5.60,
    		'keyword_id' => 62
    	]);

    	$user->live_transactions()->create([
    		'amount' => 42.55,
    		'keyword_id' => 66
    	]);

    	DB::table('days_left')->where('id', 1)->update(array('days' => 4));

    	$user->notifications()->create([
    		'title' => 'Budget Notice',
    		'description' => 'You\'ve spent over 50% of your budget in less than 3 days, careful!'
    	]);

    	$user->notifications()->create([
    		'title' => 'Purchase Notice',
    		'description' => 'Starbuck\'s is a flagged alternative but we just recorded a tranasction from there!'
    	]);

    	return response()->json([
    		'response' => 'success'
    	]);
    }

    public function simulate_step_2(Request $request)
    {
    	$user = User::find($request->input('user_id'));

    	$user->live_transactions()->create([
    		'amount' => 1.5,
    		'keyword_id' => 3
    	]);

    	$user->live_transactions()->create([
    		'amount' => 1.5,
    		'keyword_id' => 3
    	]);

    	$user->live_transactions()->create([
    		'amount' => 27,
    		'keyword_id' => 3
    	]);

    	DB::table('days_left')->where('id', 1)->update(array('days' => 0));

    	$user->notifications()->create([
    		'title' => 'Nice Work!',
    		'description' => 'We see you\'re buying Tim Horton\'s coffee now!'
    	]);

    	$user->notifications()->create([
    		'title' => 'Budget Summary',
    		'description' => 'We see you\'re buying Tim Horton\'s coffee now!'
    	]);

    	return response()->json([
    		'response' => 'success'
    	]);
    }


}
