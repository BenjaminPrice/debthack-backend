<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\Frequency;
use App\Bill;

class BillsController extends Controller
{
    public function store(Request $request)
    {
    	if ($message = $this->account_validation_failed($request->all())) {
    		return response()->json([
    			'response' => 'success',
    			'valid' => false,
    			'message' => $message
    		]);
    	} else {
    		$user = User::find($request->input('user_id'));

    		$account = $user->bills()->create([
    			'name' => $request->input('name'),
    			'amount' => $request->input('amount'),
    			'frequency' => $request->input('frequency'),
    		]);

    		return response()->json([
    			'response' => 'success',
    			'valid' => true,
    			'account' => $account
    		]);
    	}
    }

    public function get_user_bills($user_id)
    {
    	if ($user = User::find($user_id)) {
    		$bills = $user->bills;

    		for ($i = 0; $i < count($bills); $i++) {
    			$bills[$i]->frequency = Frequency::find($bills[$i]->frequency);
    		}

			return response()->json([
				'response' => 'success',
				'found' => true,
				'accounts' => $user->bills
			]);
		} else {
			return response()->json([
				'response' => 'success',
				'found' => false
			]);
		}
    }

    public function account_validation_failed($data)
    {
    	return false;
    }
}
