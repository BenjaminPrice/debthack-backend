<?php

Route::group(['prefix' => '/api'], function () {
    
	Route::group(['prefix' => 'users'], function () {
	    Route::post('create', 'UsersController@store');
		Route::get('{user_name}', 'UsersController@retrieve');
	});

	Route::group(['prefix' => 'debts'], function() {
		Route::post('create', 'DebtsController@store');
		Route::get('{user_id}', 'DebtsController@get_user_debts');
	});

	Route::group(['prefix' => 'bills'], function() {
		Route::post('create', 'BillsController@store');
		Route::get('{user_id}', 'BillsController@get_user_bills');
	});

	Route::group(['prefix' => 'income'], function() {
		Route::post('create', 'IncomesController@store');
		Route::get('{user_id}', 'IncomesController@get_user_income');
	});

	Route::group(['prefix' => 'types'], function() {
		Route::get('debts', 'TypesController@debts');
		Route::get('frequencies', 'TypesController@frequencies');
	});

	Route::group(['prefix' => 'alternatives'], function() {
		Route::get('{user_id}/new', 'AlternativesController@retrieve_new');
		Route::get('flags/{user_id}/{alternative_id}', 'AlternativesController@user_flag_exists');
		Route::post('assign', 'AlternativesController@assign_alternative');
		Route::post('ignore', 'AlternativesController@ignore_alternative');
		Route::get('user/{user_id}', 'AlternativesController@user_alternatives');
		Route::get('{user_id}', 'AlternativesController@retrieve');
	});

	Route::group(['prefix' => 'budget'], function() {
		Route::post('create_or_update', 'BudgetsController@create_or_update');
		Route::get('remaining/{user_id}', 'BudgetsController@remaining_percent');
		Route::get('days', 'BudgetsController@days_left');
		Route::get('payoff_date/{user_id}', 'BudgetsController@payoff_date');
		Route::get('{user_id}', 'BudgetsController@retrieve');
	});

	Route::group(['prefix' => 'transactions'], function() {
		Route::post('simulate_step_1', 'LiveTransactionsController@simulate_step_1');
		Route::post('simulate_step_2', 'LiveTransactionsController@simulate_step_2');
	});

	Route::group(['prefix' => 'notifications'], function() {
		Route::post('flag', 'NotificationsController@flag');
		Route::get('{user_id}', 'NotificationsController@retrieve');
	});



});

Route::get('flag', 'AlternativesController@is_ignored');

// one time use, keep them jsut incase.
// Route::get('/create_transaction_json', 'PagesController@test');
// Route::get('/open_transaction_json', 'PagesController@open');
// Route::get('/transactions', 'PagesController@transactions');