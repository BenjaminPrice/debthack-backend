<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DebtType extends Model
{
    protected $fillable = [
    	'id',
    	'desc',
    	'icon'
   	];
}
