<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Keyword extends Model
{
    protected $fillable = [
    	'name'
    ];

    public function category()
    {
    	return $this->belongsTo('App\Category');
    }

    public function transactions()
    {
    	return $this->hasMany('App\Keyword');
    }

    public function flags()
    {
        return $this->belongsToMany('App\Flag');
    }
}
