<?php

use Illuminate\Database\Seeder;
use App\Flag;
use App\Alternative;
use App\Keyword;

class FlagTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $flags = array(
        	'Starbucks' => array(
        		'alternatives' => array(
        			array(
        				'title' => 'Tim Hortons',
        				'data' => array(
        					'savings_percent' => 40,
        					'message' => 'Tim Horton\'s costs much less than Starbucks.'
        				)
        			),
        			array(
        				'title' => 'Make Coffee At Home',
        				'data' => array(
        					'savings_percent' => 60,
        					'message' => 'Save a ton of money by making your own coffee.'
        				)
        			)
        		),
        		'keywords' => array(
        			'starbucks'
        		),
        	),
        	'Loblaws' => array(
        		'alternatives' => array(
        			array(
        				'title' => 'Metro',
        				'data' => array(
        					'savings_percent' => 30,
        					'message' => 'Big savings by shopping at Metro vs other stores.'
        				)
        			)
        		),
        		'keywords' => array(
        			'sobeys',
        			'loblaw',
        			'zehrs'
        		),
        	),
        	'Casino' => array(
        		'alternatives' => array(
        			array(
        				'title' => 'Don\'t Gamble',
        				'data' => array(
        					'savings_percent' => 100,
        					'message' => 'Instead of losing money by gambling, we can instantly use that to cover debts.'
        				)
        			)
        		),
        		'keywords' => array(
        			'casino'
        		),
        	),
        );

        foreach ($flags as $key => $flag) {
        	$new_flag = Flag::create([
        		'title' => $key
        	]);

        	foreach ($flag['alternatives'] as $alternative) {
        		$new_alternative = Alternative::create([
        			'title' => $alternative['title']
        		]);

        		$new_flag->alternatives()->attach(
        			$new_alternative->id,
        			[
        				'savings_percent' => $alternative['data']['savings_percent'],
        				'message' => $alternative['data']['message']
        			]
        		);
        	}

        	foreach ($flag['keywords'] as $keyword) {
        		$new_flag->keywords()->attach(Keyword::where('name', $keyword)->first()->id);
        	}
        }
    }
}
