<?php

use Illuminate\Database\Seeder;

class IncomeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\User::first()->income()->create([
        	'name' => 'Salary',
        	'amount' => 50000,
        	'frequency' => 7
        ]);
    }
}
