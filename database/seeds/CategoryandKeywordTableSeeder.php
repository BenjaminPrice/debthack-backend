<?php

use Illuminate\Database\Seeder;
use App\Category;
use App\Keyword;

class CategoryandKeywordTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = array(
        	'Uncategorized' => array(
        		'uncategorized',
        	),
        	'Ignore' => array(
        		'payment - thank you',
        		'tax',
        		'duty',
        	),
        	'Coffee' => array(
        		'starbucks',
        		'tim hortons',
        		'second cup',
        		'cappuccino'
        	),
        	'Health' => array(
        		'pharma plus',
        		'shoppersdrugmart'
        	),
        	'Online Purchases' => array(
        		'apl itunes',
        		'eventbrite',
        		'digitalocean',
        		'spotify',
        		'godaddy.com',
        		'skype',
        		'.com'
        	),
        	'Misc' => array(
        		'equifax consumer',
        		'cdn tire',
        		'wal mart',
        		'la fitness',
        		'bww',
        		'college',
        		'magicuts',
        		'ikea',
        		'ins',
        		'cdn tire store',
        		'all seasons',
        		'travel',
        		'best buy',
        		'delta',
        		'chapters',
        		'panda',
        		'a w express',
        		'cigar',
        		'kitchen',
        		'blue mountain',
        		'HERE',
        	),
        	'Groceries' => array(
        		'sobeys',
        		'loblaw',
        		'zehrs'
        	),
        	'Restaurants' => array(
        		'wendy s toronto',
        		'restauran',
        		'subway',
        		'pizzaville',
        		'sticky fingers',
        		'hasty market',
        		'teriyaki',
        		'via panini',
        		'pizza pizza',
        		'bulk barn',
        		'burrito',
        		'pizzaiolo',
        		'panera bread',
        		'moxie s',
        		'popeye s chicken',
        		'pub',
        		'mansion keg',
        		'dominos',
        		'taco del mar',
        		'little caesars pizza',
        		'mcdonald s',
        		'7 eleven',
        		'rogers',
        		'convenience',
        		'sneaky dee s ltd',
        		'molisana',
        		'macs',
        		'hero certified burgers',
        		'freshii',
        		'wimpy s diner'
        	),
        	'Gambling' => array(
        		'casino',
        	),
        	'Alcohol' => array(
        		'the beer store',
        		'lcbo'
        	),
        	'Clothing' => array(
        		'winners',
        		'the bay',
        		'guess',
        		'aldo',
        		'under armour',
        		'ugg australia',
        		'h&m',
        		'roots',
        		'winners',
        		'topcuts'
        	),
        	'Driving' => array(
        		'uber',
        		'vinci park',
        		'taxi',
        		'toronto parking',
        		'petrocan',
        		'mac s gas bar',
        		'mac\'s',
        		'fuelco',
        		'autocare',
        		'metro parking',
        		'tire',
        		'gas bar',
        		'shell',
        		'esso'
        	),
        );

        foreach ($categories as $category => $keywords) {
        	$new_cat = Category::create([
        		'name' => $category
        	]);

        	foreach ($keywords as $keyword) {
        		$new_cat->keywords()->create([
        			'name' => $keyword
        		]);
        	}
        }
    }
}
