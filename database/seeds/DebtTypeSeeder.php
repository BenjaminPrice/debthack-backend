<?php

use Illuminate\Database\Seeder;
use App\DebtType;

class DebtTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DebtType::create([
        	'id' => 1,
        	'desc' => 'Credit Card',
        	'icon' => 'fa-credit-card'
        ]);

        DebtType::create([
        	'id' => 2,
        	'desc' => 'Mortgage',
        	'icon' => 'fa-home'
        ]);

        DebtType::create([
        	'id' => 3,
        	'desc' => 'Line of Credit',
        	'icon' => 'fa-line-chart'
        ]);

        DebtType::create([
        	'id' => 4,
        	'desc' => 'Loan',
        	'icon' => 'fa-money'
        ]);
    }
}
