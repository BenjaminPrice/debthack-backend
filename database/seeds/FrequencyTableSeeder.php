<?php

use Illuminate\Database\Seeder;
use App\Frequency;

class FrequencyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Frequency::create([
        	'id' => 1,
        	'desc' => 'Weekly'
        ]);

        Frequency::create([
        	'id' => 2,
        	'desc' => 'Bi-Weekly'
        ]);

        Frequency::create([
        	'id' => 3,
        	'desc' => 'Semi-Weekly'
        ]);

        Frequency::create([
        	'id' => 4,
        	'desc' => 'Monthly'
        ]);

        Frequency::create([
        	'id' => 5,
        	'desc' => 'Bi-Monthly'
        ]);

        Frequency::create([
        	'id' => 6,
        	'desc' => 'Quarterly'
        ]);

        Frequency::create([
        	'id' => 7,
        	'desc' => 'Annually'
        ]);
    }
}
