<?php

use Illuminate\Database\Seeder;
use App\User;

class BudgetSeeder extends Seeder
{
    protected $dividers = array(
    	1 => 52 / 52, //weekly
    	2 => 52 / 26, // bi-weekly
    	3 => 52 / 104, // semi-weekly
    	4 => 52 / 12, // monthly
    	5 => 52 / 6, // bi-monthly
    	6 => 52 / 4, // quarterly
    	7 => 52  // annually
    );

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (null === 1) {
    		return response()->json([
    			'response' => 'success',
    			'created' => false,
    			'message' => 'You must supply a user id'
    		]);
    	}

    	$user = User::find(1);

    	if (!$user) {
    		return response()->json([
    			'response' => 'success',
    			'created' => false,
    			'message' => 'User does not exist!'
    		]);
    	}

    	$weekly_income = $this->calculate_weekly_income($user->income) - $this->calculate_weekly_bills($user->bills) - $this->calculate_weekly_min_payments($user->debts);

    	if (null !== $user->budget) {
    		$user->budget()->update([
	    		'weekly_income' => $request->input('weekly_income'),
	    		'weekly_budget' => $request->input('weekly_budget'),
	    		'towards_debt' => $request->input('towards_debt'),
	    		'towards_savings' => $request->input('towards_savings'),
	    		'budget_percent' => $request->input('budget_percent'),
	    		'debt_percent' => $request->input('debt_percent')
	    	]);

	    	$budget = $user->budget;

	    	return response()->json([
	    		'response' => 'success',
	    		'type' => 'updated',
	    		'budget' => $budget
	    	]);
    	} else {
    		$budget = $user->budget()->create([
	    		'weekly_income' => $weekly_income,
	    		'weekly_budget' => $weekly_income * .25,
	    		'towards_debt' => $weekly_income * .375,
	    		'towards_savings' => $weekly_income * .375,
	    		'budget_percent' => 25,
	    		'debt_percent' => 50
	    	]);

	    	return response()->json([
	    		'response' => 'success',
	    		'type' => 'created',
	    		'budget' => $budget
	    	]);
    	}
    }

    public function calculate_weekly_income($income_sources)
    {
    	$weekly_income = 0;

    	foreach ($income_sources as $income) {
    		$weekly_income += $income->amount / $this->dividers[$income->frequency];
    	}

    	return $weekly_income;
    }

    public function calculate_weekly_bills($bills)
    {
    	$weekly_bills = 0;

    	foreach ($bills as $bill) {
    		$weekly_bills += $bill->amount / $this->dividers[$bill->frequency];
    	}

    	return $weekly_bills;
    }

    public function calculate_weekly_min_payments($debts)
    {
    	$min_payments = 0;

    	foreach ($debts as $debt) {
    		$min_payments += $debt->minPayment / $this->dividers[4];
    	}

    	return $min_payments;
    }
}
