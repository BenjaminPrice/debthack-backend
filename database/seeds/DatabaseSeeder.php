<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(FrequencyTableSeeder::class);
        $this->call(DebtTypeSeeder::class);
        $this->call(CategoryandKeywordTableSeeder::class);
        $this->call(TransactionsTableSeeder::class);
        $this->call(UserTableSeeder::class);
        $this->call(FlagTableSeeder::class);
        $this->call(BillTableSeeder::class);
        $this->call(IncomeTableSeeder::class);
        $this->call(AlternativesTableSeeder::class);
        $this->call(DaysLeftSeeder::class);
        $this->call(BudgetSeeder::class);
    }
}
