<?php

use Illuminate\Database\Seeder;

class BillTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$bills = array(
        	array(
        		'name' => 'Rent',
        		'amount' => 500,
        		'frequency' => 4
        	),
        	array(
        		'name' => 'Insurance',
        		'amount' => 200,
        		'frequency' => 4
        	),
        	array(
        		'name' => 'Gas',
        		'amount' => 100,
        		'frequency' => 4
        	),
        	array(
        		'name' => 'Groceries',
        		'amount' => 100,
        		'frequency' => 1
        	),
        	array(
        		'name' => 'Spotify',
        		'amount' => 500,
        		'frequency' => 4
        	),
        	array(
        		'name' => 'Netflix',
        		'amount' => 500,
        		'frequency' => 4
        	),
        	array(
        		'name' => 'Hydro',
        		'amount' => 75,
        		'frequency' => 4
        	)
        );

        $user = App\User::first();

        foreach ($bills as $bill) {
        	$user->bills()->create($bill);
        }
    }
}