<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAlternativeFlagPivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alternative_flag', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('flag_id')->unsigned();
            $table->foreign('flag_id')->references('id')->on('flags')->onDelete('cascade');
            $table->integer('alternative_id')->unsigned();
            $table->foreign('alternative_id')->references('id')->on('alternatives')->onDelete('cascade');
            $table->float('savings_percent');
            $table->text('message')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('alternative_flag');
    }
}
